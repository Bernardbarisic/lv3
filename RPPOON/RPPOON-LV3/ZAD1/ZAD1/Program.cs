﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZAD1
{
    class Program
    {
        //Nužno je duboko kopiranje zbog toga što je atribut data reference tipa stoga i plitka kopija će nam dati kopiju koja pokazivati na tu istu instancu

        static void Main(string[] args)
        {
            Dataset fileData = new Dataset("C:\\Users\\Bernard\\Desktop\\RPPOON\\RPPOON-LV3\\ZAD1\\ZAD1\\csv.txt");
            IList<List<string>> readonlydata = fileData.GetData();
            foreach (List<string> row in readonlydata)
            {
                foreach (string data in row)
                {
                    Console.WriteLine(data);
                }
            }

            Dataset fileData2 = (Dataset)fileData.Clone();
            IList<List<string>> readonlydata2 = fileData2.GetData();
            foreach (List<string> row in readonlydata2)
            {
                foreach (string data in row)
                {
                    Console.WriteLine(data);
                }
            }
            fileData.ClearData();
            readonlydata2 = fileData2.GetData();
            Console.WriteLine("Provjera je li fileData2 ostao isti:");
            foreach (List<string> row in readonlydata2)
            {
                foreach (string data in row)
                {
                    Console.WriteLine(data);
                }
            }
        }
    }
}
