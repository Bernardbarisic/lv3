﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZAD2
{
    class Program
    {
        static void Main(string[] args)
        {
            MatrixGenerator generator = MatrixGenerator.GetInstance();
            double[][] matrix = generator.NextMatrix(3, 4);
            foreach (double[] row in matrix)
            {
                foreach (double element in row)
                {
                    Console.Write(element + " ");
                }
                Console.WriteLine();
            }
        }
    }
}
