﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZAD4
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime timestamp = DateTime.Now;

            ConsoleNotification notification = new ConsoleNotification("Bernard", "New message", "Hello.", timestamp, Category.INFO, ConsoleColor.Red);
            NotificationManager manager = new NotificationManager();

            manager.Display(notification);
            notification = new ConsoleNotification("Bernard", "New message", "Hello.", timestamp, Category.ALERT, ConsoleColor.Blue);
            manager.Display(notification);
            notification = new ConsoleNotification("Bernard", "New message", "Hello.", timestamp, Category.ERROR, ConsoleColor.DarkBlue);
            manager.Display(notification);
        }
    }
}
